package com.inspearit.sample.linker.dd;

public class ExistedTransfer {

	public ExistedTransfer() {
		//this is a blank constructor 
	}

	public boolean send() {
		return true;
	}

	public void setData(DDData data) {
		//set data to old style
		//let send() use
		
	}

	public DDData getData() {
		//get data as old way
		//compose DDData
		return new DDData();
	}

}
