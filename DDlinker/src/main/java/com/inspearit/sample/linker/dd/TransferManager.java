package com.inspearit.sample.linker.dd;

public class TransferManager {
	protected static String transferType;

	public static Transfer getTransfer() {
		Transfer transfer;
		// create transfer according to config
		if ("NewA".equals(transferType)) {
			// if config 1
			transfer = new NewATransfer();
		} else {
			// else if config 2
			transfer = new DefaultTransfer();
		}
		// set the transfer before using it
		// transfer.set(blabla)
		return transfer;
	}

	public static Transfer getTransfer(String transferType) {
		TransferManager.transferType = transferType;
		return getTransfer();
	}

	private TransferManager() {
		//hide public one
	}
}
