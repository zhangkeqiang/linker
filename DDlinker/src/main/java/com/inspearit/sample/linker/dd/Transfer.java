package com.inspearit.sample.linker.dd;

public interface Transfer {

	boolean sendData(DDData data);
	DDData receiveData();
}
