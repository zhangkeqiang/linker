package com.inspearit.sample.linker.dd;

public class DefaultTransfer implements Transfer {
	protected ExistedTransfer oldTransfer;
	public DefaultTransfer() {
		oldTransfer = new ExistedTransfer();
	}

	public boolean sendData(DDData data) {
		//sendData by specific ways by oldTransfer
		//1, set data to oldTran
		oldTransfer.setData(data);
		//such as call restfulapi, store db, or store file		
		return oldTransfer.send();
	}

	public DDData receiveData() {
		//maybe some timer usage
		return oldTransfer.getData();
	}

}
