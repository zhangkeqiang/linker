package com.inspearit.sample.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JdbcUrl {
	static Logger logger = LogManager.getLogger(JdbcUrl.class);
	HashMap<String, String> map = new HashMap<>();
	Properties properties = new Properties();

	public JdbcUrl() {
		initProperies();
		if (properties.getProperty("ENV_ID") == null) {
			// no ENV_ID setting
			map.put("DB_URL", "JNDISetting");
		} else {
			// if ENV_ID exists, sync the setting from properties
			syncMapandProperties();
		}
	}

	private void syncMapandProperties() {
		Iterator<Entry<Object, Object>> it = properties.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Object, Object> entry = it.next();
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();
			map.put(key, value);
		}

	}

	public Map getJdbcConfigMap() {
		return map;
	}

	private void initProperies() {
		try {
			logger.debug("load properties");
			properties.load(getClass().getResourceAsStream("/config.properties"));
		} catch (IOException ex) {
			logger.error(ex.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
