package com.inspearit.sample.linker.dd;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TransferManagerTest {

	Transfer transfer ;
	@Before
	public void setUp() throws Exception {
		transfer = TransferManager.getTransfer();
		assertNotNull(transfer);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTransferManagerSend1() {
		DDData data = new DDData();
		//data is composed by business logic
		assertTrue(transfer.sendData(data));
		
	}
	
	@Test
	public void testTransferManagerReceive1() {
		DDData data = transfer.receiveData();
		assertNotNull(data);
		
	}
	
	@Test
	public void testNewATransfer() {
		Transfer newATransfer = TransferManager.getTransfer("NewA");
		DDData data =  new DDData();

		assertNotNull(data);
		newATransfer.sendData(data);
		
	}

}
