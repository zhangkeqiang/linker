package com.inspearit.sample.config;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JdbcUrlTest {
	static Logger logger = LogManager.getLogger();
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		//fffff
	}

	@Test
	public void testJdbcUrl() {
		JdbcUrl url = new JdbcUrl();
		Map map = url.getJdbcConfigMap();
		assertNotNull(map);
		String envId = (String) map.get("ENV_ID");
		assertNotNull(envId);
		logger.info("Env is "+envId);
		if (envId.equals("dev")) {
			assertEquals("dev-jdbc:oracle:thin:@172.21.129.51:1521:orcl", map.get("DB_URL"));
			assertEquals("zhangsan", map.get("DB_USERNAME"));
			assertEquals("qwe34fghhhss", map.get("DB_PASSWORD"));
			assertEquals("dev", map.get("ENV_ID"));
		}else if(envId.equals("local")) {
			assertEquals("local-jdbc:oracle:thin:@localhost:1521:XE", map.get("DB_URL"));
			assertEquals("user1", map.get("DB_USERNAME"));
			assertEquals("***", map.get("DB_PASSWORD"));
			assertEquals("local", map.get("ENV_ID"));
		}
	}
}
