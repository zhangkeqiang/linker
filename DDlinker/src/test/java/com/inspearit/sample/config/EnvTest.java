package com.inspearit.sample.config;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class EnvTest {
	static Logger logger = LogManager.getLogger();
	@Test
	public void testSystemProperties() {
		Properties properties = System.getProperties();
		Iterator<Entry<Object, Object>> it = properties.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Object, Object> entry = it.next();
			Object key = entry.getKey();
			Object value = entry.getValue();
			logger.warn("key:" + key + "-----" + "value :" + value);
		}
	}

	@Test
	public void testSystemProperties_JavaHome() {
		Properties properties = System.getProperties();
		String javaHome = properties.getProperty("java.home");
		logger.warn("Java Home is " + javaHome);
		assertTrue(javaHome.indexOf("jdk")>0);
	}
}
